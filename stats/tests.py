from django.test import TestCase, Client
from .api import GovernmentApi
from .views import covid_update, covid_yearly, covid_monthly, covid_daily
import time
import json

class GovernmentApiCacheUpdateUnitTest(TestCase):

    def setUp(self):
        self.governmentApi = GovernmentApi("https://data.covid19.go.id/public/api", 5)
    
    def test_cache_mechanism_should_work(self):

        self.assertTrue(self.governmentApi.is_cache_expired())
        _, _ = self.governmentApi.get_covid_update()
        self.assertFalse(self.governmentApi.is_cache_expired()) # cached

        time.sleep(6)
        self.assertTrue(self.governmentApi.is_cache_expired()) # cache expired
    
    def test_get_covid_update_should_work(self):
    
        res, status = self.governmentApi.get_covid_update()
        self.assertEquals(status, 200)

        self.assertTrue('update' in  res)
        self.assertTrue('total' in res['update'])
        self.assertTrue('penambahan' in res['update'])

class GovernmentApiYearlyUnitTest(TestCase):

    # to activate caching mechanism
    governmentApi = GovernmentApi("https://data.covid19.go.id/public/api", 20) 

    def test_covid_yearly_should_work(self):
        res, status = self.governmentApi.get_covid_yearly()
        self.assertEquals(status, 200)

        self.assertEquals(res[0].year, "2020")
        self.assertEquals(res[-1].year, "2022")

    def test_covid_yearly_since_should_work(self):

        res, status = self.governmentApi.get_covid_yearly(since = 2021)
        self.assertEquals(status, 200)

        self.assertEquals(res[0].year, "2021")
    
    def test_covid_yearly_upto_should_work(self):
        
        res, status = self.governmentApi.get_covid_yearly(upto = 2020)
        self.assertEquals(status, 200)

        self.assertEquals(res[-1].year, "2020")

    def test_covid_yearly_since_upto_should_work(self):
        res, status = self.governmentApi.get_covid_yearly(since=2021, upto = 2021)
        self.assertEquals(status, 200)
        self.assertEquals(res[0].year, "2021")

class GovernmentApiMonthlyUnitTest(TestCase):

    # to activate caching mechanism
    governmentApi = GovernmentApi("https://data.covid19.go.id/public/api", 20) 

    def test_covid_monthly_should_work(self):
        res, status = self.governmentApi.get_covid_monthly()
        self.assertEquals(status, 200)
        self.assertEqual(res[0].month, "2020-03")
        self.assertEqual(res[-1].month, "2022-01")

    def test_covid_monthly_since_should_work(self):

        res, status = self.governmentApi.get_covid_monthly(since="2021-04")
        self.assertEquals(status, 200)
        self.assertEqual(res[0].month, "2021-04")
    
    def test_covid_monthly_upto_should_work(self):
        res, status = self.governmentApi.get_covid_monthly(upto="2021-08")
        self.assertEquals(status, 200)
        self.assertEqual(res[-1].month, "2021-08")

    def test_covid_monthly_since_upto_should_work(self):
        res, status = self.governmentApi.get_covid_monthly(since="2020-08", upto="2021-04")
        self.assertEquals(status, 200)
        self.assertEqual(res[0].month, "2020-08")
        self.assertEqual(res[-1].month, "2021-04")

    def test_covid_monthly_with_year_should_work(self):
        res, status = self.governmentApi.get_covid_monthly(since="2021-02", upto="2021-05", year_in = "2021")
        self.assertEquals(status, 200)
        self.assertEqual(res[0].month, "2021-02")
        self.assertEqual(res[-1].month, "2021-05")

    def test_covid_monthly_with_year_month_should_work(self):
        res, status = self.governmentApi.get_covid_monthly(year_in = "2021", month_in = "05")
        self.assertEquals(status, 200)
        self.assertEqual(res[0].month, "2021-05")
        self.assertEqual(res[-1].month, "2021-05")

class GovernmentApiDailyUnitTest(TestCase):

    governmentApi = GovernmentApi("https://data.covid19.go.id/public/api", 20) 

    def test_covid_daily_should_work(self):
        res, status = self.governmentApi.get_covid_daily()
        self.assertEquals(status, 200)
        self.assertEqual(res[0].date, "2020-03-02")
    
    def test_covid_daily_since_should_work(self):
        res, status = self.governmentApi.get_covid_daily(since="2021-03-09")
        self.assertEquals(status, 200)
        self.assertEqual(res[0].date, "2021-03-09")
    
    def test_covid_daily_upto_should_work(self):
        res, status = self.governmentApi.get_covid_daily(upto="2021-05-10")
        self.assertEquals(status, 200)
        self.assertEqual(res[-1].date, "2021-05-10")
    
    def test_covid_daily_since_upto_should_work(self):
        res, status = self.governmentApi.get_covid_daily(since="2020-10-18", upto="2021-05-10")
        self.assertEquals(status, 200)
        self.assertEqual(res[0].date, "2020-10-18")
        self.assertEqual(res[-1].date, "2021-05-10")
    
    def test_covid_daily_with_year_should_work(self):
        res, status = self.governmentApi.get_covid_daily(since="2021-10-18", year_in = "2021")
        self.assertEquals(status, 200)
        self.assertEqual(res[0].date, "2021-10-18")
        self.assertEqual(res[-1].date, "2021-12-31")

    def test_covid_daily_with_year_month_should_work(self):
        res, status = self.governmentApi.get_covid_daily(upto="2021-07-11", year_in = "2021", month_in = "07")
        self.assertEquals(status, 200)
        self.assertEqual(res[0].date, "2021-07-01")
        self.assertEqual(res[-1].date, "2021-07-11")
    
    def test_covid_daily_with_year_month_date_should_work(self):
        res, status = self.governmentApi.get_covid_daily(year_in = "2021", month_in = "07", day_in = "05")
        self.assertEquals(status, 200)
        self.assertEqual(res[0].date, "2021-07-05")
        self.assertEqual(res[-1].date, "2021-07-05")

class CovidUpdateViewsUnitTest(TestCase):

    def setUp(self):
        self.client = Client()

    def test_covid_update_should_return_success(self):
        response = self.client.get('')

        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertTrue("data" in response)
    

class YearlyCovidViewsUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
    
    def test_valid_base_route_should_work(self):
        response = self.client.get('/yearly?since=2021')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(response["data"][0]["year"], "2021")
    
    def test_invalid_year_format_should_fail(self):
        response = self.client.get('/yearly?since=202a')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])

    def test_invalid_year_format_2_should_fail(self):
        response = self.client.get('/yearly?upto=202a')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])

    def test_valid_year_route_should_work(self):
        response = self.client.get('/yearly/2021')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(response["data"][0]["year"], "2021")
    
    def test_valid_year_route_2_should_work(self):
        response = self.client.get('/yearly/2021?upto=2021')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(response["data"][0]["year"], "2021")
    
    def test_invalid_year_param_should_work(self):
        response = self.client.get('/yearly/2021?upto=2020')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])

    def test_invalid_since_upto_should_fail(self):
        response = self.client.get('/yearly?since=2022&upto=2021')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])

class MonthlyCovidViewsUnitTests(TestCase):

    def setUp(self):
        self.client = Client()
    
    def test_valid_base_route_should_work(self):
        response = self.client.get('/monthly?since=2020.07&upto=2021.05')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(response["data"][0]["month"], "2020-07")
        self.assertEquals(response["data"][-1]["month"], "2021-05")
    
    def test_invalid_since_upto_should_fail(self):
        response = self.client.get('/monthly?since=2020.07&upto=2020.06')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])
    
    def test_invalid_base_route_should_fail(self):
        response = self.client.get('/monthly?since=20a1.03')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])
    
    def test_invalid_base_route_2_should_fail(self):
        response = self.client.get('/monthly?since=2021.3')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])

        response = self.client.get('/monthly?upto=201.03')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])

    def test_valid_with_year_should_work(self):
        response = self.client.get('/monthly/2020?since=2020.07&upto=2021.05')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(response["data"][0]["month"], "2020-07")
        self.assertEquals(response["data"][-1]["month"], "2020-12")
    
    def test_invalid_with_year_should_fail(self):
        response = self.client.get('/monthly/20a0?since=2020.07&upto=2021.05')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])
    
    def test_invalid_with_year_2_should_fail(self):
        response = self.client.get('/monthly/20a0?since=2020-07&upto=2021-05')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])
    
    def test_valid_with_year_month_should_work(self):
        response = self.client.get('/monthly/2020/08')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(response["data"][0]["month"], "2020-08")
        self.assertEquals(response["data"][-1]["month"], "2020-08")
    
    def test_valid_with_year_month_should_work(self):
        response = self.client.get('/monthly/2020/08?since=2021.09')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(len(response["data"]), 0)
    
    def test_invalid_with_year_month_should_work(self):
        response = self.client.get('/monthly/2020/8')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])
    
    def test_invalid_with_year_month_2_should_work(self):
        response = self.client.get('/monthly/2020/a8')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])

class DailyCovidViewsUnitTest(TestCase):

    def setUp(self):
        self.client = Client()

    def test_base_url_should_work(self):
        response = self.client.get('/daily?since=2020.07.03&upto=2021.05.06')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(response["data"][0]["date"], "2020-07-03")
        self.assertEquals(response["data"][-1]["date"], "2021-05-06")
    
    def test_base_url_invalid_since_should_fail(self):
        response = self.client.get('/daily?since=2020-07-03')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])
    
    def test_base_url_invalid_upto_should_fail(self):
        response = self.client.get('/daily?upto=2020-07-03')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])
    
    def test_base_url_invalid_since_upto_range_should_fail(self):
        response = self.client.get('/daily?since=2020.07.03&upto=2020.07.02')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])
    
    def test_valid_route_with_year_should_work(self):
        response = self.client.get('/daily/2020?since=2020.07.03&upto=2020.11.30')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(response["data"][0]["date"], "2020-07-03")
        self.assertEquals(response["data"][-1]["date"], "2020-11-30")
    
    def test_invalid_route_with_year_should_work(self):
        response = self.client.get('/daily/202a?since=2020.07.03&upto=2020.11.30')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])
    
    def test_valid_route_with_year_2_should_work(self):
        response = self.client.get('/daily/2021?since=2020.07.03&upto=2020.11.30')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(len(response["data"]), 0)
    
    def test_valid_route_with_year_month_should_work(self):
        response = self.client.get('/daily/2020/07?upto=2020.07.17')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(response["data"][0]["date"], "2020-07-01")
        self.assertEquals(response["data"][-1]["date"], "2020-07-17")

    def test_valid_route_with_year_month_2_should_work(self):
        response = self.client.get('/daily/2020/08?upto=2020.07.17')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(len(response["data"]), 0)
    
    def test_invalid_route_with_year_month_should_work(self):
        response = self.client.get('/daily/2020/a7?upto=2020.07.17')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])
    
    def test_valid_route_with_year_month_date_should_work(self):
        response = self.client.get('/daily/2020/07/04')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(response["data"][0]["date"], "2020-07-04")
        self.assertEquals(response["data"][-1]["date"], "2020-07-04")

    def test_valid_route_with_year_month_date_2_should_work(self):
        response = self.client.get('/daily/2020/08/10?upto=2020.07.17')
        response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(response["ok"])
        self.assertEquals(len(response["data"]), 0)
    
    def test_invalid_route_with_year_month_date_should_work(self):
        response = self.client.get('/daily/2020/07/b7')
        response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(response["ok"])
