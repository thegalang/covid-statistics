import requests
from datetime import datetime, timedelta
from .models import CovidYearly, CovidMonthly, CovidDaily

# hit government COVID api
class GovernmentApi:

    # base_url: recieve data from where?
    # cache_ttl: how many seconds does the cache last?
    def __init__(self, base_url, cache_ttl):
        self.cache_ttl = cache_ttl 
        self.BASE_URL = base_url

         # minimalist caching mechanism. Will delete cache after cache_ttl seconds
        self.covid_update_json_cache = None
        self.last_retrieved_time = None

   
    
    def is_cache_expired(self):
        if self.last_retrieved_time is None:
            return True
        
        current_time = datetime.now()
        return current_time - timedelta(seconds=self.cache_ttl) > self.last_retrieved_time

    # returns response in dictionary form and status_code
    def get_covid_update(self):

        r = None
        if self.is_cache_expired():
            r = requests.get(f"{self.BASE_URL}/update.json", timeout=10)
            if r.status_code == 200:
                self.covid_update_json_cache = r.json()
                self.last_retrieved_time = datetime.now()
                return r.json(), r.status_code
            else:
                return None, r.status_code
        else:
            return self.covid_update_json_cache, 200

    def get_covid_yearly(self, since = 2020, upto = 2022):

        data, status_code = self.get_covid_update()
        if status_code != 200:
            return None, status_code
        
        ret = {str(year) : CovidYearly(str(year), 0, 0, 0, 0) for year in range(since, upto+1)}

        for covid_daily in data["update"]["harian"]:
            statistic_date = covid_daily["key_as_string"].split('T')[0]
            statistic_year = statistic_date.split('-')[0]
            
            positive =  covid_daily["jumlah_positif"]["value"]
            recovered = covid_daily["jumlah_sembuh"]["value"]
            deaths = covid_daily["jumlah_meninggal"]["value"]
            active = covid_daily["jumlah_positif_kum"]["value"] - covid_daily["jumlah_sembuh_kum"]["value"] - covid_daily["jumlah_meninggal_kum"]["value"]
            if since <= int(statistic_year) <= upto:
                newStat = CovidYearly(str(statistic_year), positive, recovered, deaths, active)
                ret[statistic_year].update(newStat)

        
        return list(ret.values()), 200
    
    def get_covid_monthly(self, since = "2020-03", upto = "2022-01", year_in = None, month_in = None):

        data, status_code = self.get_covid_update()
        if status_code != 200:
            return None, status_code
        
        ret = {}

        for covid_daily in data["update"]["harian"]:
            statistic_date = covid_daily["key_as_string"].split('T')[0]
            year, month, _ = statistic_date.split('-')
            statistic_month = f"{year}-{month}"

            # check if its in the specified year and month
            if (year_in is not None and year != year_in) or (month_in is not None and month != month_in):
                continue
            
            positive =  covid_daily["jumlah_positif"]["value"]
            recovered = covid_daily["jumlah_sembuh"]["value"]
            deaths = covid_daily["jumlah_meninggal"]["value"]
            active = covid_daily["jumlah_positif_kum"]["value"] - covid_daily["jumlah_sembuh_kum"]["value"] - covid_daily["jumlah_meninggal_kum"]["value"]

            if since <= statistic_month <= upto:
                if statistic_month not in ret:
                    ret[statistic_month] = CovidMonthly(statistic_month, 0, 0, 0, 0)

                ret[statistic_month].update(CovidMonthly(statistic_month, positive, recovered, deaths, active))
        
        return list(ret.values()), 200

    def get_covid_daily(self, since = "2020-03-01", upto = "2022-01-31", year_in = None, month_in = None, day_in = None):
    
        data, status_code = self.get_covid_update()
        if status_code != 200:
            return None, status_code
        
        ret = {}

        for covid_daily in data["update"]["harian"]:
            statistic_date = covid_daily["key_as_string"].split('T')[0]
            year, month, day = statistic_date.split('-')
            statistic_day = f"{year}-{month}-{day}"

            # check if its in the specified year and month
            if (year_in is not None and year != year_in) or (month_in is not None and month != month_in) or (day_in is not None and day != day_in):
                continue
            
            positive =  covid_daily["jumlah_positif"]["value"]
            recovered = covid_daily["jumlah_sembuh"]["value"]
            deaths = covid_daily["jumlah_meninggal"]["value"]
            active = covid_daily["jumlah_positif_kum"]["value"] - covid_daily["jumlah_sembuh_kum"]["value"] - covid_daily["jumlah_meninggal_kum"]["value"]

            if since <= statistic_day <= upto:
                if statistic_day not in ret:
                    ret[statistic_day] = CovidDaily(statistic_day, 0, 0, 0, 0)

                ret[statistic_day].update(CovidDaily(statistic_day, positive, recovered, deaths, active))
        
        return list(ret.values()), 200