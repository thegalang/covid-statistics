from rest_framework import serializers

class CovidUpdateSerializer(serializers.Serializer):
    total_positive = serializers.IntegerField()
    total_recovered = serializers.IntegerField()
    total_deaths = serializers.IntegerField()
    total_active = serializers.IntegerField()
    new_positive = serializers.IntegerField()
    new_recovered = serializers.IntegerField()
    new_deaths = serializers.IntegerField()
    new_active = serializers.IntegerField()

class CovidYearlySerializer(serializers.Serializer):
    year = serializers.CharField()
    positive = serializers.IntegerField()
    recovered = serializers.IntegerField()
    deaths = serializers.IntegerField()
    active = serializers.IntegerField()

class CovidMonthlySerializer(serializers.Serializer):
    month = serializers.CharField()
    positive = serializers.IntegerField()
    recovered = serializers.IntegerField()
    deaths = serializers.IntegerField()
    active = serializers.IntegerField()

class CovidDailySerializer(serializers.Serializer):
    date = serializers.CharField()
    positive = serializers.IntegerField()
    recovered = serializers.IntegerField()
    deaths = serializers.IntegerField()
    active = serializers.IntegerField()