import json
import os
from datetime import timedelta, date
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
import requests
from django.views import View
from .serializers import CovidUpdateSerializer, CovidYearlySerializer, CovidMonthlySerializer, CovidDailySerializer
from .models import CovidUpdate, CovidYearly
from .api import GovernmentApi
import re

governmentApi = GovernmentApi('https://data.covid19.go.id/public/api', 3600)

# regexes for verification
YYYY_RE = "[0-9]{4}"
YYYYMM_RE = "[0-9]{4}\.[0-9]{2}"
YYYYMMDD_RE = "[0-9]{4}\.[0-9]{2}\.[0-9]{2}"
MM_RE = "[0-9]{2}"
DD_RE = "[0-9]{2}"

# wrap a JSON inside the custom response on API contract
def wrap_response(ok, data, message):
    return {
        "ok": ok,
        "data": data,
        "message": message
    }

@api_view(('GET',))
def covid_update(request):
    if request.method == 'GET':
        data, status_code = governmentApi.get_covid_update()
        if status_code == 200:
            
            total_stats = data["update"]["total"]
            new_stats = data["update"]["penambahan"]
            covid_update = CovidUpdate(total_stats["jumlah_positif"], total_stats["jumlah_sembuh"], total_stats["jumlah_meninggal"],
                                        new_stats["jumlah_positif"], new_stats["jumlah_sembuh"], new_stats["jumlah_meninggal"])

            serializer = CovidUpdateSerializer(covid_update)
            
            return Response(wrap_response(True, serializer.data, "success"), status=status.HTTP_200_OK)
        else:
            return Response(wrap_response(False, None, "Fail to get data from Government API"), status=status_code)

@api_view(('GET',))
def covid_yearly(request, year = None):

    if request.method == 'GET':
        year_since = 2020
        year_upto = 2022

        if year is not None:
            year_since = year
            year_upto = year

        # get query params
        if "since" in request.GET:
            try:
                year_since = max(int(request.GET.get("since")), year_since)
            except:
                return Response(wrap_response(False, None, "Invalid value for [since] parameter"), status=400)
        
        if "upto" in request.GET:
            try:
                year_upto = min(int(request.GET.get("upto")), year_upto)
            except:
                return Response(wrap_response(False, None, "Invalid value for [upto] parameter"), status=400)
        
        

        if year_since > year_upto:
            return Response(wrap_response(False, None, "Invalid value for parameters, must satisfy [since] < [upto]"), status=400)

        
        data, status_code = governmentApi.get_covid_yearly(year_since, year_upto)
        if status_code == 200:
            
            serializer = CovidYearlySerializer(data, many = True)

            return Response(wrap_response(True, serializer.data, "success"), status=status.HTTP_200_OK)
        
        else:
            return Response(wrap_response(False, None, "Fail to get data from Government API"), status=status_code)

# helper function to check if a string matches the given regex pattern
def is_match_regex(string, pattern):
    matched = re.match(pattern, string)
    is_match = bool(matched)
    return is_match

@api_view(('GET',))
def covid_monthly(request, year = None, month = None):
    
    if request.method == 'GET':
        month_since = '2020.03'
        month_upto = '2022.01'

        # get query params
        if "since" in request.GET:
            month_since = request.GET.get("since")
            if not is_match_regex(month_since, YYYYMM_RE):
                return Response(wrap_response(False, None, "Invalid value for [since] parameter"), status=400)
        
        if "upto" in request.GET:
            month_upto = request.GET.get("upto")
            if not is_match_regex(month_upto, YYYYMM_RE):
                return Response(wrap_response(False, None, "Invalid value for [upto] parameter"), status=400)
        
        if month_since > month_upto:
            return Response(wrap_response(False, None, "Invalid value for parameters, must satisfy [since] < [upto]"), status=400)

        # from url paths. Separate variable because must be stricly in here
        year_in = None
        if year is not None:
            if not is_match_regex(year, YYYY_RE):
                return Response(wrap_response(False, None, "Invalid value for route <year>"), status=400)
    
            year_in = year
        
        month_in = None
        if month is not None:
            if not is_match_regex(month, MM_RE):
                return Response(wrap_response(False, None, "Invalid value for route <month>"), status=400)
            
            month_in = month

        # converts since and upto from YYYY.MM to YYYY-MM
        month_since = month_since.replace('.', '-')
        month_upto = month_upto.replace('.', '-')

        data, status_code = governmentApi.get_covid_monthly(month_since, month_upto, year_in, month_in)
        if status_code == 200:
            
            serializer = CovidMonthlySerializer(data, many = True)

            return Response(wrap_response(True, serializer.data, "success"), status=status.HTTP_200_OK)
        
        else:
            return Response(wrap_response(False, None, "Fail to get data from Government API"), status=status_code)

@api_view(('GET',))
def covid_daily(request, year = None, month = None, day = None):
    
    if request.method == 'GET':
        day_since = '2020.03.01'
        day_upto = '2022.01.30'

        # get query params
        if "since" in request.GET:
            day_since = request.GET.get("since")
            if not is_match_regex(day_since, YYYYMMDD_RE):
                return Response(wrap_response(False, None, "Invalid value for [since] parameter"), status=400)
        
        if "upto" in request.GET:
            day_upto = request.GET.get("upto")
            if not is_match_regex(day_upto, YYYYMMDD_RE):
                return Response(wrap_response(False, None, "Invalid value for [upto] parameter"), status=400)
        
        if day_since > day_upto:
            return Response(wrap_response(False, None, "Invalid value for parameters, must satisfy [since] < [upto]"), status=400)

        # from url paths. Separate variable because must be stricly in here
        year_in = None
        if year is not None:
            if not is_match_regex(year, YYYY_RE):
                return Response(wrap_response(False, None, "Invalid value for route <year>"), status=400)
    
            year_in = year
        
        month_in = None
        if month is not None:
            if not is_match_regex(month, MM_RE):
                return Response(wrap_response(False, None, "Invalid value for route <month>"), status=400)
            
            month_in = month
        
        day_in = None
        if day is not None:
            if not is_match_regex(day, MM_RE):
                return Response(wrap_response(False, None, "Invalid value for route <day>"), status=400)
            
            day_in = day

        # converts since and upto from YYYY.MM to YYYY-MM
        day_since = day_since.replace('.', '-')
        day_upto = day_upto.replace('.', '-')

        data, status_code = governmentApi.get_covid_daily(day_since, day_upto, year_in, month_in, day_in)
        if status_code == 200:
            
            serializer = CovidDailySerializer(data, many = True)

            return Response(wrap_response(True, serializer.data, "success"), status=status.HTTP_200_OK)
        
        else:
            return Response(wrap_response(False, None, "Fail to get data from Government API"), status=status_code)