from django.db import models

# class to hold general covid statistics
class CovidUpdate:
    
    def __init__(self, total_positive, total_recovered, total_deaths, 
                 new_positive, new_recovered, new_deaths ):
        
        self.total_positive = total_positive
        self.total_recovered = total_recovered
        self.total_deaths = total_deaths
        self.total_active = total_positive - total_deaths - total_recovered
        self.new_positive = new_positive
        self.new_recovered = new_recovered
        self.new_deaths = new_deaths
        self.new_active = new_positive - new_deaths - new_recovered

# class to hold yearly covid statistics
class CovidYearly:
    def __init__(self, year, positive, recovered, deaths, active):
        self.year = year
        self.positive = positive
        self.recovered = recovered
        self.deaths = deaths
        self.active = active
    
    # other is the more recent data
    def update(self, other):
        self.positive += other.positive
        self.recovered += other.recovered
        self.deaths += other.deaths
        self.active = other.active
        

class CovidMonthly:
    def __init__(self, month, positive, recovered, deaths, active):
        self.month = month
        self.positive = positive
        self.recovered = recovered
        self.deaths = deaths
        self.active = active
    
    # other is the more recent data
    def update(self, other):
        self.positive += other.positive
        self.recovered += other.recovered
        self.deaths += other.deaths
        self.active = other.active

class CovidDaily:
    def __init__(self, date, positive, recovered, deaths, active):
        self.date = date
        self.positive = positive
        self.recovered = recovered
        self.deaths = deaths
        self.active = active
    
    # other is the more recent data
    def update(self, other):
        self.positive += other.positive
        self.recovered += other.recovered
        self.deaths += other.deaths
        self.active = other.active