from rest_framework import routers
from .views import covid_update, covid_yearly, covid_monthly, covid_daily
from django.urls import path

urlpatterns = [
    path('', covid_update),
    path('yearly', covid_yearly),
    path('yearly/<int:year>', covid_yearly),
    path('monthly', covid_monthly ),
    path('monthly/<str:year>', covid_monthly),
    path('monthly/<str:year>/<str:month>', covid_monthly),
    path('daily', covid_daily ),
    path('daily/<str:year>', covid_daily),
    path('daily/<str:year>/<str:month>', covid_daily),
    path('daily/<str:year>/<str:month>/<str:day>', covid_daily),
]

