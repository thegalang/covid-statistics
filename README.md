
Pipelines: 
main [![pipeline status](https://gitlab.com/thegalang/covid-statistics/badges/main/pipeline.svg)](https://gitlab.com/thegalang/covid-statistics/-/commits/main)
dev [![pipeline status](https://gitlab.com/thegalang/covid-statistics/badges/main/pipeline.svg)](https://gitlab.com/thegalang/covid-statistics/-/commits/dev)

Coverages:
main [![coverage report](https://gitlab.com/thegalang/covid-statistics/badges/main/coverage.svg)](https://gitlab.com/thegalang/covid-statistics/-/commits/main)
dev [![coverage report](https://gitlab.com/thegalang/covid-statistics/badges/main/coverage.svg)](https://gitlab.com/thegalang/covid-statistics/-/commits/dev)

# covid-statistics

A covid statistics API using https://data.covid19.go.id/public/update.json

Downloading the data on every hit is a bottleneck, so the API uses a **caching mechanism** to cache the downloaded JSON for one hour to improve speed. The caching mechanism used is a simple in-memory storage where it invalidates on new hit. For the current usecase, it is not neccessary yet to use advanced noSQL for caching such as redis, because all of the data can be found in that one simple JSON file.

The API can be optimized further by creating a database for the "harian" of the json so that queries can be done quickly. We can create a cron job to update the database daily when a new entry appears. However, for this usecase it is overkill and a simple loop is still sufficiently fast.

## How to Build and Run

Build and run the docker container

```
docker build --tag covid-statistics:latest .
docker run --name covid-statistics -p 8000:8000 covid-statistics:latest
```

## Download from DockerHub

```
docker pull thegalang/covid-statistics:1.0.0
docker run --name covid-statistics -p 8000:8000 covid-statistics:1.0.0
```

## Using the API

Download Postman (https://www.postman.com/) and import `example_postman_collection.json`. This will list all availble routes and query parameters implemented on this app.

For example:
```
curl --location --request GET 'localhost:8000/monthly/2020?since=2020.05'
```

will give response:
```
{
    "ok": true,
    "data": [
        {
            "month": "2020-05",
            "positive": 16355,
            "recovered": 5786,
            "deaths": 821,
            "active": 17552
        },
        {
            "month": "2020-06",
            "positive": 29912,
            "recovered": 17498,
            "deaths": 1263,
            "active": 28703
        },
        {
            "month": "2020-07",
            "positive": 51991,
            "recovered": 41101,
            "deaths": 2255,
            "active": 37338
        },
        {
            "month": "2020-08",
            "positive": 66420,
            "recovered": 60052,
            "deaths": 2286,
            "active": 41420
        },
        {
            "month": "2020-09",
            "positive": 112212,
            "recovered": 88988,
            "deaths": 3323,
            "active": 61321
        },
        {
            "month": "2020-10",
            "positive": 123080,
            "recovered": 122854,
            "deaths": 3129,
            "active": 58418
        },
        {
            "month": "2020-11",
            "positive": 128795,
            "recovered": 112717,
            "deaths": 3076,
            "active": 71420
        },
        {
            "month": "2020-12",
            "positive": 204315,
            "recovered": 160579,
            "deaths": 5193,
            "active": 109963
        }
    ],
    "message": "success"
}
```